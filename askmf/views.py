from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Todo_Form
from .models import Todo

# Create your views here.
response = {}
def index(request):
    response['author'] = "Hilya Auli Fesmia" #TODO Implement yourname
    todo = Todo.objects.all()
    response['todo'] = todo
    html = 'lab_5/lab_5.html'
    response['todo_form'] = Todo_Form
    return render(request, html, response)

def add_todo(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['untuk'] = request.POST['untuk']
        response['dari'] = request.POST['dari']
        response['description'] = request.POST['description']
        todo = Todo(untuk=response['untuk'],dari=response['dari'],description=response['description'])
        todo.save()
        return HttpResponseRedirect('/askmf/')
    else:
        return HttpResponseRedirect('/askmf/')
