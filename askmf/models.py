from django.db import models

class Todo(models.Model):
    untuk = models.TextField(default='')
    dari = models.TextField(default='')
    description = models.TextField(default='')
    created_date = models.DateTimeField(auto_now_add=True)
