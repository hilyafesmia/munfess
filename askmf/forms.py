from django import forms

class Todo_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    untuk_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 1,
        'class': 'todo-form-input',
        'placeholder':'Tujuan'
    }
    dari_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 1,
        'class': 'todo-form-input',
        'placeholder':'Inisial kamu'
    }
    description_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'Masukan pesanmu'
    }

    untuk = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=untuk_attrs))
    dari = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=dari_attrs))
    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
